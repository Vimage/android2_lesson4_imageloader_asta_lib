package com.example.dimon.lesson4_imageloader_asta_lib;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {


    ImageView image;
    Button btnAsyncTask;

    final static String IMAGE_URL = "http://hdoboi.net/uploads/hd/95263_krasivyiy_osenniy_peyzaj.jpg";
    final int PROGRESS_DLG_ID = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);



        btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
        image = (ImageView) findViewById(R.id.imageView);

    }

    @Override
    protected Dialog onCreateDialog(int dialogId) {
        ProgressDialog progress = null;
        switch (dialogId) {
            case PROGRESS_DLG_ID:
                progress = new ProgressDialog(this);
                progress.setMessage("Loading...");

                break;
        }
        return progress;
    }


    public void onbtnAsyncTaskClick(View button) {
         new LoadImageTask().execute(IMAGE_URL);
    }


    public void onbtnFrescoClick(View button) {
        Uri uri = Uri.parse(IMAGE_URL);
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.my_image_view);
        draweeView.setImageURI(uri);
    }


    class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            publishProgress(new Void[]{});

            String url = "";
            if (params.length > 0) {
                url = params[0];
            }

            InputStream input = null;

            try {
                URL urlConn = new URL(url);
                input = urlConn.openStream();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return BitmapFactory.decodeStream(input);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            showDialog(PROGRESS_DLG_ID);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            dismissDialog(PROGRESS_DLG_ID);
            image.setImageBitmap(result);
        }
    }

}
